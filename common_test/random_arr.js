/**
 * 
 * @param {number} start 
 * @param {number} end 
 * @param {number} count
 * @returns {number[]} 
 */
function random_arr(start, end, count)
{
  const result = [];
  const diff = end - start;
  for (let i = 0; i < count; i++)
  {
    const ran = Math.floor(Math.random() * diff + start);
    result.push(ran);
  }
  return result;
}

module.exports = random_arr;