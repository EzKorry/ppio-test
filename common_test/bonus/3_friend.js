/**
 *
 * @param {string[]} users
 * @returns {string[]}
 */
module.exports = function (users) {
  return users.filter((user) => user.length === 4);
};
