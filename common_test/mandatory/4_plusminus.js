/**
 *
 * @param {number} n
 * @returns {string}
 */
 module.exports = function (n) {
  if (n < 10 || n > 10000)
    return 'error';
  return 'apple';
};
