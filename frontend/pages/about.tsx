export default function About() {
  return (
    <div className="about pt-5">
      <h2 className="text-2xl font-bold leading-7 text-gray-900 sm:text-3xl sm:truncate mb-4">
        AWESOME FOOD STORE
      </h2>
      <p className="text-gray-700 text-sm">
        이 프로젝트는 맛집 리스트를 보여주고 리스트 중 하나의 아이템을 선택하면
        팝업으로 상세 설명을 보여줍니다.
      </p>
    </div>
  );
}
