/**
 *
 * @param {[number, number, number]} input
 */
module.exports = function (input) {
  if (input?.length !== 3) {
    return -1;
  }
  if (input[0] > input[1] && input[0] > input[2]) {
    return input[1] < input[2] ? 2 : 1;
  }
  if (input[0] < input[1] && input[0] < input[2]) {
    return input[1] > input[2] ? 2 : 1;
  }
  return 0;
};
