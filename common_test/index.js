const range = require("./range");
const random_arr = require("./random_arr");
const man1 = require("./mandatory/1_bungi");
const man2 = require("./mandatory/2_smallest");
const man3 = require("./mandatory/3_winpoint");
const man4 = require("./mandatory/4_plusminus");
const bonus1 = require("./bonus/1_vowel");
const bonus2 = require("./bonus/2_middle");
const bonus3 = require("./bonus/3_friend");
const bonus4 = require("./bonus/4_iq");
const test_func = (func, arg1) => {
  console.log(`${arg1} ==> ${func(arg1)}`);
}

console.log("-- TEST MANDATORY 1 --");
range(1, 13).forEach((i) => {
  console.log(`${i} ==> ${man1(i)}`);
});

console.log("-- TEST MANDATORY 2 --");
const test2 = (arr) => {
  console.log(`[${arr.join(",")}] ==> ${man2(arr)}`);
};
test2(random_arr(0, 10, 5).concat([0, 1, 2]));
test2(random_arr(0, 10, 5).concat([0, 1, 2]));
test2(random_arr(0, 10, 5).concat([0, 1, 2]));
test2(random_arr(0, 3, 20));

console.log("-- TEST MANDATORY 3 --");
for (let i = 0; i < 5; i++) {
  const arr = [];
  for (let j = 0; j < 10; j++) {
    arr.push(
      `${Math.floor(Math.random() * 5)}:${Math.floor(Math.random() * 5)}`
    );
  }
  console.log(`${arr.map((item) => `(${item})`).join(",")} ==> ${man3(arr)}`);
}

console.log("-- TEST MANDATORY 4 --");
// 두 자리 양수에서, 각 자리수를 더한 수를 본래의 수에서 빼면 무조건 9의 배수가 나옵니다.
// 100 이상의 수는 무조건 99가 나옵니다. 이 또한 9의 배수입니다.
// 보기에서 9의 배수는 모두 'apple'이므로 무조건 'apple'을 출력합니다.
test_func(man4, 10);
test_func(man4, 13);
test_func(man4, 73);
test_func(man4, 107);
test_func(man4, 582);
console.log("-- TEST BONUS 1 --");
console.log(`${"abracadabra"} ==> ${bonus1("abracadabra")}`);
console.log(`${"abr sd acee o bra"} ==> ${bonus1("abr sd acee o bra")}`);

console.log("-- TEST BONUS 2 --");
test_func(bonus2, [1, 3, 5]);
test_func(bonus2, [1, 5, 3]);
test_func(bonus2, [3, 1, 5]);
test_func(bonus2, [3, 5, 1]);
test_func(bonus2, [5, 1, 3]);
test_func(bonus2, [5, 3, 1]);
test_func(bonus2, [3, 3, 3]);
test_func(bonus2, [1, 3, 3]);
test_func(bonus2, [3, 1, 3]);
test_func(bonus2, [3, 3, 1]);

console.log("-- TEST BONUS 3 --");
test_func(bonus3, ["Ryan", "Kieran", "Mark"]);
test_func(bonus3, ["아", "안녕", "안녕하", "안녕하세", "안녕하세요", "황보민아"]);

console.log("-- TEST BONUS 4 --");
// 짝수, 홀수가 하나씩 있을 때에도 그냥 짝수의 pos 만을 리턴합니다. 
test_func(bonus4, "2 4 7 8 10");
test_func(bonus4, "1 2 1 1");
test_func(bonus4, "2 3");
test_func(bonus4, "3 2");
test_func(bonus4, "2 3 4 5");
