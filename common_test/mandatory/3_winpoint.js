/**
 *
 * @param {string[]} games
 * @returns {number}
 */
module.exports = function (games) {
  let score = 0;
  games.forEach((game) => {
    const splitted = game.split(":");
    if (splitted.length !== 2) return;
    const my = parseInt(splitted[0], 10);
    const you = parseInt(splitted[1], 10);
    if (my > you) score += 3;
    else if (my === you) score += 1;
  });
  return score;
};
