import { useEffect } from "react";
import { useRouter } from "next/router";

export default function Custom404() {
  const router = useRouter();
  useEffect(() => {
    const moveToHomeTimeout = setTimeout(() => {
      router.push("/");
    }, 5000);
    return () => {
      clearTimeout(moveToHomeTimeout);
    };
  });

  return (
    <div className="flex flex-grow flex-col items-center justify-center">
      <div className="bg-white px-4 py-16 sm:px-6 sm:py-24 md:grid md:place-items-center lg:px-8">
        <div className="max-w-max mx-auto">
          <main className="sm:flex">
            <p className="text-4xl font-extrabold text-gray-600 sm:text-5xl">
              404
            </p>
            <div className="sm:ml-6">
              <div className="sm:border-l sm:border-gray-200 sm:pl-6">
                <h1 className="text-3xl font-extrabold text-gray-900 tracking-tight sm:text-4xl mb-3">
                  페이지를 찾을 수 없습니다.
                </h1>
                <p className="mt-1 text-sm text-gray-500">
                  url이 맞는지 확인해주세요. 5초 후 홈페이지로 이동합니다.
                </p>
              </div>
            </div>
          </main>
        </div>
      </div>
    </div>
  );
}
