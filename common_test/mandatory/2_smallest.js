/**
 *
 * @param {number[]} ids
 */
module.exports = function (ids) {
  ids.push(-1);
  ids.sort();
  let result = ids[ids.length - 1] + 1;
  let next = 0;
  ids.some((id, index, arr) => {
    next = arr[index + 1];
    if (typeof next === "number" && id + 1 < next) {
      result = id + 1;
      return 1;
    }
    return 0;
  });
  return result;
};
