import { MailIcon, PhoneIcon } from "@heroicons/react/solid";
import Image from "next/image";
import axios from "axios";
import {
  ComponentElement,
  Fragment,
  ReactComponentElement,
  useEffect,
  useState,
} from "react";
import { Dialog, Transition } from "@headlessui/react";
import { CheckIcon } from "@heroicons/react/outline";
import Modal from "react-modal";
import { XIcon } from "@heroicons/react/solid";

// global axios 충돌 방지
const axiosLocal = axios.create();

interface Store {
  id: number;
  name: string;
  description: string;
  image: string;
  thumb: string;
  divided_descriptions?: string[];
  url?: string;
}

export default function Store() {
  const [stores, setStores] = useState<Store[]>([]);
  const [openedId, setOpenedId] = useState(-1);
  const [modalTransitionShow, setModalTransitionShow] = useState(false);

  useEffect(() => {
    (async () => {
      // fetch data from server
      const res = await axiosLocal.get("http://localhost:9000/stores");
      let stores: Store[] = res.data;
      stores = stores.map((store) => ({
        ...store,
        divided_descriptions: store.description
          .split("\n")
          .map((desc) => desc.trim()),
      }));
      setStores(stores);

      // Modal settings
      Modal.setAppElement("body");
    })();
  }, []);

  const modalRequestClose = () => {
    setOpenedId(-1);
    setModalTransitionShow(false);
  };

  return (
    <div>
      <div className="grid grid-cols-2 gap-4 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-5 xl:grid-cols-6">
        {stores.map((store) => (
          <div key={store.id}>
            <div className="rounded-xl overflow-hidden">
              <a href="#">
                <Image
                  src={store.thumb}
                  alt={store.description}
                  width={1}
                  height={1}
                  layout="responsive"
                  className="transform hover:scale-110 hover:opacity-60 transition "
                  onClick={() => {
                    setOpenedId(store.id);
                  }}
                ></Image>
              </a>
            </div>

            <Modal
              // shouldCloseOnOverlayClick
              preventScroll
              onAfterOpen={() => setModalTransitionShow(true)}
              onRequestClose={modalRequestClose}
              closeTimeoutMS={200}
              className="inline-block relative max-h-screen  overflow-auto text-left transform transition-all sm:my-8 sm:align-middle sm:max-w-sm sm:w-full"
              style={{
                overlay: {
                  position: "fixed",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  backgroundColor: "rgba(0, 0, 0, 0.5)",
                },
              }}
              isOpen={openedId === store.id}
              aria={{
                labelledby: `heading_${store.id}`,
                describedby: `full_description_${store.id}`,
              }}
            >
              <Transition
                show={modalTransitionShow}
                className="bg-white transform shadow-xl rounded-lg overflow-hidden"
                as="div"
                enter="ease-out duration-300"
                enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                enterTo="opacity-100 translate-y-0 sm:scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              >
                {
                  // close Button
                }
                <div className="absolute right-0 top-0 mt-5 mr-5 z-10">
                  <button
                    className="rounded-md  inline-flex text-white hover:text-gray-300 
                          focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                    onClick={modalRequestClose}
                  >
                    <span className="sr-only">Close</span>
                    <XIcon
                      className="h-7 w-7 drop-shadow-2xl"
                      aria-hidden="true"
                    />
                  </button>
                </div>
                {
                  // content
                }
                <div>
                  <Image
                    src={store.image}
                    alt={store.description}
                    width={1}
                    height={1}
                    layout="responsive"
                  ></Image>
                  <div className="p-4 sm:p-6">
                    <h3
                      id={`heading_${store.id}`}
                      className="text-lg leading-6 font-medium text-gray-900 mb-2"
                    >
                      {store.name}
                    </h3>
                    <div
                      className="text-sm text-gray-500 mb-1"
                      id={`full_description_${store.id}`}
                    >
                      {store.divided_descriptions?.map((desc, desc_index) => (
                        <p key={desc_index}>{desc}</p>
                      ))}
                    </div>
                    {store.url && (
                      <p>
                        <a
                          href={store.url}
                          target="_blank"
                          rel="noreferrer"
                          className="underline text-sm text-blue-600 hover:opacity-80"
                        >
                          홈페이지 바로가기
                        </a>
                      </p>
                    )}
                  </div>
                </div>
              </Transition>
            </Modal>
          </div>
        ))}
        <div></div>
      </div>
    </div>
  );
}
