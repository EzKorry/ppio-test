/**
 * @typedef {Object} CheckItem
 * @property {number} index
 * @property {number} value
 */

/**
 *
 * @param {string} numbers
 */
module.exports = function (numbers) {
  /** @type {CheckItem[]} */
  const evens = [];

  /** @type {CheckItem[]} */
  const odds = [];

  const arr = numbers.split(' ').map((num) => parseInt(num, 10));
  arr.forEach((num, index) => {
    if (num % 2 == 0) {
      evens.push({ index, value: num });
    } else {
      odds.push({ index, value: num });
    }
  });
  if (evens.length === 1)
    return evens[0].index + 1;
  else if (odds.length === 1)
    return odds[0].index + 1;
  return -1;
};
