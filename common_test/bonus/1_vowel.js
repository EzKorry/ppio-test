const vowels = ["a", "e", "i", "o", "u"];

/**
 *
 * @param {string} str
 * @returns {number}
 */
module.exports = function (str) {
  let count = 0;
  for (let i = 0; i < str.length; i++) {
    if (vowels.includes(str[i])) count += 1;
  }
  return count;
};
