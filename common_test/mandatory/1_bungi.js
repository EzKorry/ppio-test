/**
 * 
 * @param {number} month 
 */
module.exports = function (month) {
  if (month < 1 || month > 12) {
    return -1;
  }

  return Math.floor((month - 1) / 3) + 1;
};
